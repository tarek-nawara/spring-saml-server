package com.ejada.springsamlserver.model;

public class LoginFilter {
    private String username;
    private String password;

    public LoginFilter() {
    }

    public LoginFilter(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "LoginFilter{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
