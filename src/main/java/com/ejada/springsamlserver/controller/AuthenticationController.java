package com.ejada.springsamlserver.controller;

import com.ejada.springsamlserver.model.LoginFilter;
import com.ejada.springsamlserver.service.AuthnResponseBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthenticationController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AuthnResponseBuilderService authnResponseBuilderService;

    @PostMapping("/authenticate")
    public String authenticate(@ModelAttribute("SAMLRequest") String request,
                               @ModelAttribute("RelayState") String replayState,
                               Model model) {
        String samlResponse = authnResponseBuilderService.buildAuthnResponse();
        logger.info("SAMLRequest={}", request);
        logger.info("RelayState={}", replayState);
        model.addAttribute("SAMLResponse", samlResponse);
        model.addAttribute("RelayState", replayState);
        model.addAttribute("loginFilter", new LoginFilter());
        return "authentication-form";
    }
}
