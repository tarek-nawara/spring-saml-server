package com.ejada.springsamlserver.service;

public interface AuthnResponseBuilderService {
    /**
     * Build the authentication request which will
     * be sent to the IDP.
     *
     * @return authentication request
     */
    String buildAuthnResponse();
}
