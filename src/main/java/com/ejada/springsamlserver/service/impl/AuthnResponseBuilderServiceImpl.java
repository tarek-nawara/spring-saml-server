package com.ejada.springsamlserver.service.impl;

import com.ejada.springsamlserver.service.AuthnResponseBuilderService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.nio.charset.Charset;

@Service
public class AuthnResponseBuilderServiceImpl implements AuthnResponseBuilderService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String buildAuthnResponse() {
        String samlResponse = "this is a dummy saml response";
        logger.info("Current time={}", System.currentTimeMillis());
        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("Response.xml");
            samlResponse = IOUtils.toString(inputStream, Charset.defaultCharset());
        } catch (Exception e) {
            logger.warn("Failed to read response, exception={}", e);
        }
        byte[] encoded = Base64.encodeBase64(samlResponse.getBytes());
        samlResponse = new String(encoded);
        return samlResponse;
    }
}
