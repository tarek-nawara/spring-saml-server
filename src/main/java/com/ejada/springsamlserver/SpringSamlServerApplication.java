package com.ejada.springsamlserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSamlServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSamlServerApplication.class, args);
    }
}
