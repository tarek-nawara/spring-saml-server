package com.ejada.springsamlserver.model;

public class EncodedAuthnRequest {
    private String encodedAuthnRequest;

    public String getEncodedAuthnRequest() {
        return encodedAuthnRequest;
    }

    public void setEncodedAuthnRequest(String encodedAuthnRequest) {
        this.encodedAuthnRequest = encodedAuthnRequest;
    }
}
